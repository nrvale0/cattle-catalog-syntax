# -*- coding: utf-8 -*-
"""
"""

def setup(
        name='cattle-catalog-syntax',
        version='0.0.dev1',
        description='Rancher Cattle Catalog syntax checker',
        long_description='Various tooling for doing syntax checking of Rancher Cattle Catalogs.',
        url='https://github.com/nrvale0/cattle-catalog-syntax',
        author='Nathan Valentine <nrvale0@gmail.com>',
        license='Apache 2.0',
        classifiers=[
            'Devlopment Status :: 3 - Alpha',
            'Intended Audience :: Devlopers',
            'License :: OSI Approved :: Apache 2.0',
            'Programming Language :: Python 3'],
        keywords='rancher cattle catalog development',
        packages=find_packages(excludes=['contrib', 'docs', 'tests']),
        install_requires=['invoke'],
        ):
    """
    """
    pass
